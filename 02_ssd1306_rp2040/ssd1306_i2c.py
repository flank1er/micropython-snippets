# driver OLED display SSD1306 with I2C bus, resolution 128x32, i2c address 0x3C
# for WeAct Raspberry Pi Pico Board with MicroPython (rp2040)
# used pins: 9 - SCL, 8 - SDA (I2C_0)
# License: MIT
# Author www.count-zero.ru (flanker)

from micropython import const
import array 
from machine import Pin, I2C
import cybercafe_16x8 as cf

I2C_ADR = const(0x3C)
LCD_OFF = const(0xAE)
LCD_ON = const(0xAF)
LEN = const(512)

class SSD1306():
    def __init__(self):
        self.power = True
        self.init_display()

    def init_display(self):
        self.i2c = I2C(0, scl=Pin(9), sda=Pin(8), freq=400000)
        init = array.array('B',[0xAE,0xD5,0x80,0xA8,0x1F,0xD3,0x00,0x00,0x8D,0x14,0x20,0x00,0xA1,0xC8,0xDA,0x02,0x81,0x8F,0xD9,0xF1,0xDB,0x40,0xA4,0xA6,0xAF]) 
        for it in init:
            self.send_cmd(it)

    def is_ready(self):
        r=self.i2c.scan()
        for it in r:
            if it == I2C_ADR:
                return True
        return False

    def send_cmd(self,value):
        var = bytearray(1)
        var[0]=value
        self.i2c.writeto_mem(I2C_ADR, 0, var)

    def send_data(self,value):
        var = bytearray(1)
        var[0]=value
        self.i2c.writeto_mem(I2C_ADR, 0x40, var)        

    def power_on(self):    
        if (self.power == True):            
            return            
        self.send_cmd(LCD_ON)
        self.power=True

    def power_off(self):    
        if (self.power == False):           
            return            
        self.send_cmd(LCD_OFF)
        self.power=False

    def set_hw_cursor(self,x,y):
        if (self.power == False):
            return
        self.send_cmd(0x22)
        self.send_cmd(y)
        self.send_cmd(0xff)
        self.send_cmd(0x21)
        self.send_cmd(x)
        self.send_cmd(127)

    def fill(self,value):
        if (self.power == False):
            return
        self.set_hw_cursor(0,0)
        for i in range(LEN):
            self.send_data(value)

    def send_char(self,x,y,ch):
        if (self.power == False):
            return        
        if (ch >= 0x20):
            a,b=cf.get_char(ch-0x20)
        else:
            return
        self.set_hw_cursor(x,y)
        for i in range(8):
            self.send_data(a[i])
        self.set_hw_cursor(x,y+1)
        for i in range(8,16):
            self.send_data(a[i])  

    def print_str(self,x,y,str):
        if (self.power == False):
            return
        y=y*2
        if (y > 2 or y < 0):
            return
        l=len(str)
        for i in range(l):
            j=x+i*8
            self.send_char(j,y,ord(str[i]))                      

